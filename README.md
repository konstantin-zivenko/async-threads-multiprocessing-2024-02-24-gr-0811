# 005_006_asyncio_treading_multiprocessing_GIL

[video 1](https://youtu.be/TeJe1SccSd0)

[video 2](https://youtu.be/DsXaQ-74p7M)

# Поєднання двох лекцій:
# Асінхронне програмування у Python
# Багатопоточне програмування у Python

---

### программа навчання: **Python Advanced 2022**

### номер заняття: 5, 6

### засоби навчання: Python; інтегроване середовище розробки (PyCharm або Microsoft Visual Studio + Python Tools for Visual Studio + можливість використання Юпітер Jupyter Notebook)

---

### Огляд, мета та призначення уроку

